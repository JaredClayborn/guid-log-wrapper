# Guid Log Wrapper

A wrapper around the console for javascript.
This is intended to be used with a logging system to keep track of frontend logs
This will add "Tracking GUID: " and a GUID so you can easily follow the path on the frontend.
There is a getCurrentGuid function so that you can pass it to the backend to continue tracking a single user through your system if you choose


## Installation

```javascript
npm install guid-log-wrapper
```

## Usage
```javascript
import { log, getCurrentGuid } from 'guid-log-wrapper';

log('Some error happend!', 'error') // second parameter can be 'log', 'info', 'warn', or 'error'. defaults to 'log'
// expected output is a console error with "Some error happend! Tracking GUID: <some guid>
console.log(getCurrentGuid())
// expected output is a console log with the guid used in the message above
```