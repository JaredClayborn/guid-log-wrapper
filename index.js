const easyGuidGenerator = require('easy-guid-generator')

let currentGuid

const _ensureGuid = () => {
    if (!currentGuid) {
        currentGuid = easyGuidGenerator.generateGuid()
    }
}

exports.getCurrentGuid = () => {
    _ensureGuid()
    return currentGuid
}

exports.log = (message, type) => {
    _ensureGuid()
    let mess = message + ` Tracking GUID: ${currentGuid}`
    switch (type) {
        case 'log': 
            console.log(mess)
            break
        case 'warn':
            console.warn(mess)
            break
        case 'error': 
            console.error(mess)
            break
        case 'info':
            console.info(mess)
            break
        default:
            console.log(mess)
    }
}

// exports.log = log
// exports.getCurrentGuid = getCurrentGuid